<?php

namespace App\Tests\Command;

use App\Application\Service\Crontab\CrontabService;
use App\Application\Service\Helper\LogWriterService;
use App\Infrastructure\Symfony\Command\CrontabCommand;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class CrontabCommandTest
 * @package App\Tests\Command
 */
class CrontabCommandTest extends KernelTestCase
{
    /** @var CommandTester */
    private $commandTester;

    protected function setUp(): void
    {
        $loggerInterface = $this->createMock(LoggerInterface::class);
        $logWriterService = new LogWriterService($loggerInterface);

        $crontabService = $this->createMock(CrontabService::class);

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $application->add(new CrontabCommand($crontabService, $logWriterService));
        $command = $application->find(CrontabCommand::getDefaultName());
        $this->commandTester = new CommandTester($command);
    }

    public function testExecute(): void
    {
        $this->commandTester->execute([]);
        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString(CrontabCommand::MESSAGE_SUCCESS_CRONTAB_COMMAND, $output);
    }
}