<?php

namespace App\Tests\Command;

use App\Application\Service\Helper\DatetimeHelper;
use App\Application\Service\Helper\LogWriterService;
use App\Application\Service\Helper\PathService;
use App\Infrastructure\Symfony\Command\LogWriterCommand;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class LogWriterCommandTest
 * @package App\Tests\Command
 */
class LogWriterCommandTest extends KernelTestCase
{
    public const PATH_LOG_CRONTAB = '/var/log/crontab-';
    public const FILE_LOG_TERMINATION = '.log';

    /** @var CommandTester */
    private $commandTester;

    /** @var DatetimeHelper */
    private $datetimeHelper;

    /** @var PathService */
    private $pathService;

    protected function setUp(): void
    {
        $loggerInterface = $this->createMock(LoggerInterface::class);
        $logWriterService = new LogWriterService($loggerInterface);

        $this->datetimeHelper = new DatetimeHelper();

        $kernel = static::createKernel();
        $this->pathService = new PathService($kernel);
        $application = new Application($kernel);
        $application->add(new LogWriterCommand($logWriterService, $this->datetimeHelper));
        $command = $application->find(LogWriterCommand::getDefaultName());
        $this->commandTester = new CommandTester($command);
        LogWriterCommand::getDefaultName();
    }

    public function testExecute(): void
    {
        $this->commandTester->execute([]);
        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString(LogWriterCommand::MESSAGE_SUCCESS_LOG_WRITER_COMMAND, $output);
    }

    /**
     * @throws \Exception
     */
    public function testLogWriterDate(): void
    {
        $this->commandTester->execute([]);
        $dateFormatYmd = $this->datetimeHelper->getCurrentDateTimeAsString('Y-m-d');
        $this->assertFileExists(
            $this->pathService->getBasePath().self::PATH_LOG_CRONTAB.$dateFormatYmd.self::FILE_LOG_TERMINATION
        );

    }
}