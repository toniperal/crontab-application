function createCron() {

    let crontab = document.getElementById('crontab_timeCrontab').value;
    let element = document.getElementById('content');
    let id_crontab = element.getAttribute('data-idcrontab');

    checkCrontab(crontab);

    const xml = new XMLHttpRequest();
    xml.open("POST", endpoint_crontab_activate, true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.onload = function () {
        const dataReply = JSON.parse(this.responseText);

        if (dataReply.processed) {

            let buttonDelete = ' </br><div class="led-box">';
            buttonDelete += '<div class="led-green"></div>';
            buttonDelete += '<button type="button" class="btn btn-danger btn-sm" id="delete_cron" onclick="deleteCron()">Desactivar cron</button>';
            buttonDelete += '</div>';

            document.getElementById("status_cron").innerHTML = buttonDelete;
            alert('Configuración activada con exito');

        } else {
            alert('Hubo un error, refresque la pagina...si el problema persiste contacte con nosotros');
        }
    }
    const parameters = {
        'crontab': crontab,
        'id_crontab': id_crontab
    }
    xml.send(JSON.stringify(parameters));
}

function checkCrontab(crontab) {
    let e = crontab.split(/(\s+)/).filter(function (e) {
        return e.trim().length > 0;
    });

    if (crontab === '') {
        alert('La configuración Crontab no puede estar vacía');
        return;
    }

    if (e.length > 5) {
        alert('Solo 5 parametros de configuración');
        return;
    }

    if (e.length < 5) {
        alert('Se requieren 5 parametros de configuración');
        return;
    }

    if (!isCronValidMinutes(e[0])) {
        alert('El minuto o minutos no son válidos');
        return;
    }

    if (!isCronValidhour(e[1])) {
        alert('la hora o horas no son válidas');
        return;
    }

    if (!isCronValidmonthDay(e[2])) {
        alert('Los días  o el día del mes no son válidos');
        return;
    }

    if (!isCronValidmonth(e[3])) {
        alert('El mes o meses no so válidos');
        return;
    }

    if (!isCronValidweekDay(e[4])) {
        alert('La semana o semanas no son válidas');
        return;
    }
}

function deleteCron() {

    let element = document.getElementById('content');
    let id_crontab = element.getAttribute('data-idcrontab');

    let bool = confirm("Si elimina el crontab se parada la ejecución para este commando, ¿ Continuar ?");
    if (bool === true) {

        const xml = new XMLHttpRequest();
        xml.open("POST", endpoint_crontab_deactivate, true);
        xml.setRequestHeader("Content-Type", "application/json");
        xml.onload = function () {
            const dataReply = JSON.parse(this.responseText);

            if (dataReply.processed) {

                let button = ' </br><div class="led-box">';
                button += '<div class="led-red"></div>';
                button += '<button type="button" class="btn btn-info btn-sm" id="create_cron" onclick="createCron()">Activar cron</button>';
                button += '</div>';

                document.getElementById("status_cron").innerHTML = button;
                alert('Configuración desativada con exito');

            } else {
                alert('Hubo un error, refresque la pagina...si el problema persiste contacte con TIC');
            }
        }
        const parameters = {
            'id_crontab': id_crontab
        }
        xml.send(JSON.stringify(parameters));
    }
}

function isCronValidMinutes(crontab) {

    // minute: 0-59
    let minutes = new RegExp(/^(\*|[1-5]?[0-9](-[1-5]?[0-9])?)(\/[1-9][0-9]*)?(,(\*|[1-5]?[0-9](-[1-5]?[0-9])?)(\/[1-9][0-9]*)?)*$/);
    return minutes.test(crontab);
}

function isCronValidhour(crontab) {

    let hour = new RegExp(/^(\*|(1?[0-9]|2[0-3])(-(1?[0-9]|2[0-3]))?)(\/[1-9][0-9]*)?(,(\*|(1?[0-9]|2[0-3])(-(1?[0-9]|2[0-3]))?)(\/[1-9][0-9]*)?)*$/);
    return hour.test(crontab);
}

function isCronValidmonthDay(crontab) {

// monthDay: 1-31
    let monthDay = new RegExp(/^(\*|([1-9]|[1-2][0-9]?|3[0-1])(-([1-9]|[1-2][0-9]?|3[0-1]))?)(\/[1-9][0-9]*)?(,(\*|([1-9]|[1-2][0-9]?|3[0-1])(-([1-9]|[1-2][0-9]?|3[0-1]))?)(\/[1-9][0-9]*)?)*$/);
    return monthDay.test(crontab);
}

function isCronValidmonth(crontab) {

// month: 1-12
    let month = new RegExp(/^(\*|([1-9]|1[0-2]?)(-([1-9]|1[0-2]?))?)(\/[1-9][0-9]*)?(,(\*|([1-9]|1[0-2]?)(-([1-9]|1[0-2]?))?)(\/[1-9][0-9]*)?)*$/);
    return month.test(crontab);
}

function isCronValidweekDay(crontab) {

// weekDay: 0-6
    let weekDay = new RegExp(/^(\*|[0-6](-[0-6])?)(\/[1-9][0-9]*)?(,(\*|[0-6](-[0-6])?)(\/[1-9][0-9]*)?)*$/);
    return weekDay.test(crontab);
}