# App Crontab
## The application must be able to take a configuration file (crontab style) and run commands on demand.

Created by: [Toni Peral](https://bitbucket.org/toniperal/)

Email: [toniperalf@gmail.com](mailto:toniperalf@gmail.com)

* [Setup](doc/Setup.md)
* [Start](doc/Start.md)
