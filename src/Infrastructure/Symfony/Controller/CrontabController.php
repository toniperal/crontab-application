<?php

namespace App\Infrastructure\Symfony\Controller;

use App\Application\Manager\CrontabManager;
use App\Domain\Entity\Crontab;
use App\Infrastructure\Symfony\Form\CrontabType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/crontab")
 */
class CrontabController extends AbstractController
{
    /** @var CrontabManager */
    private $crontabManager;

    public function __construct(CrontabManager $crontabManager)
    {
        $this->crontabManager = $crontabManager;
    }

    /**
     * @Route("/", name="crontab_index", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        return $this->render(
            'crontab/index.html.twig',
            [
                'crontabs' => $this->crontabManager->findAll(),
            ]
        );
    }

    /**
     * @Route("/new", name="crontab_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $crontab = $this->crontabManager->create();
        $form = $this->createForm(CrontabType::class, $crontab);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $crontab->setStatusCrontab(Crontab::STATE_DEACTIVATE_CRONTAB);
            $crontab->setTimeCrontab($crontab->getTimeCrontab());
            $this->crontabManager->save($crontab);

            return $this->redirectToRoute('crontab_index');
        }

        return $this->render(
            'crontab/new.html.twig',
            [
                'crontab' => $crontab,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="crontab_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Crontab $crontab
     * @return Response
     */
    public function edit(Request $request, Crontab $crontab): Response
    {
        $form = $this->createForm(CrontabType::class, $crontab);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->crontabManager->save($crontab);

            return $this->redirectToRoute('crontab_index');
        }

        return $this->render(
            'crontab/edit.html.twig',
            [
                'crontab' => $crontab,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/activate", name="crontab_activate")
     * @param Request $request
     * @return JsonResponse
     */
    public function activateCrontab(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        $cron = htmlentities($content->crontab);
        $idCrontab = htmlentities($content->id_crontab);

        /** @var Crontab $crontab */
        $crontab = $this->crontabManager->findOneBy(['id' => $idCrontab]);

        $pass = false;

        if ($cron && $crontab) {
            $crontab->setTimeCrontab($cron);
            $crontab->setStatusCrontab(Crontab::STATE_ACTIVATE_CRONTAB);
            $this->crontabManager->save($crontab);
            $pass = true;
        }

        return new JsonResponse(['processed' => $pass], 200);
    }

    /**
     * @Route("/deactivate", name="crontab_deactivate")
     * @param Request $request
     * @return JsonResponse
     */
    public function deactivateCrontab(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        $idCrontab = $content->id_crontab;

        /** @var Crontab $crontab */
        $crontab = $this->crontabManager->findOneBy(['id' => $idCrontab]);

        $pass = false;
        if ($crontab) {

            $crontab->setTimeCrontab('');
            $crontab->setStatusCrontab(Crontab::STATE_DEACTIVATE_CRONTAB);
            $this->crontabManager->save($crontab);

            $pass = true;
        }

        return new JsonResponse(['processed' => $pass], 200);
    }

    /**
     * @Route("/{id}", name="crontab_delete", methods={"DELETE","POST"})
     * @param Request $request
     * @param Crontab $crontab
     * @return Response
     */
    public function delete(Request $request, Crontab $crontab): Response
    {
        if ($this->isCsrfTokenValid('delete'.$crontab->getId(), $request->request->get('_token'))) {
            $this->crontabManager->remove($crontab);
        }

        return $this->redirectToRoute('crontab_index');
    }
}