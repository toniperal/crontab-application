<?php

namespace App\Infrastructure\Symfony\Command;

use App\Application\Exception\LogWriterException;
use App\Application\Service\Helper\DatetimeHelper;
use App\Application\Service\Helper\LogWriterService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LogWriterCommand
 * @package App\Infrastructure\Symfony\Command
 */
class LogWriterCommand extends Command
{
    protected static $defaultName = 'log:writer:command';
    public const MESSAGE_SUCCESS_LOG_WRITER_COMMAND = 'Log written successfully.';

    /** @var LogWriterService */
    private $logWriterService;

    /** @var DatetimeHelper */
    private $datetimeHelper;

    /**
     * LogWriterCommand constructor.
     * @param LogWriterService $logWriterService
     * @param DatetimeHelper $datetimeHelper
     * @param string|null $name
     */
    public function __construct
    (
        LogWriterService $logWriterService,
        DatetimeHelper $datetimeHelper,
        string $name = null
    ) {
        parent::__construct($name);
        $this->logWriterService = $logWriterService;
        $this->datetimeHelper = $datetimeHelper;
    }

    protected function configure()
    {
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws LogWriterException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {

            $this->executeLogCrontab();
            $output->writeln(self::MESSAGE_SUCCESS_LOG_WRITER_COMMAND);
            return 0;

        } catch (LogWriterException $e) {
            $this->logWriterService->logError($e->getMessage());
            throw $e;
        }
    }

    /**
     * @throws LogWriterException
     */
    private function executeLogCrontab(): void
    {
        try {
            $this->logWriterService->logCrontab(
                LogWriterService::MESSAGE_LOG_WRITER_WITH_TIME_AND_DAY.$this->datetimeHelper->getCurrentDateTimeAsString()
            );

        } catch (\Exception $e) {
            throw new LogWriterException(LogWriterException::LOG_WRITER_COMMAND_MESSAGE.$e->getMessage());
        }
    }
}