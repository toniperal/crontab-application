<?php

namespace App\Infrastructure\Symfony\Command;

use App\Application\Exception\CrontabException;
use App\Application\Service\Crontab\CrontabService;
use App\Application\Service\Helper\LogWriterService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CrontabCommand
 * @package App\Infrastructure\Symfony\Command
 */
class CrontabCommand extends Command
{
    protected static $defaultName = 'crontab:command';
    public const MESSAGE_SUCCESS_CRONTAB_COMMAND = 'Crontab successfully.';

    /** @var CrontabService */
    private $crontabService;

    /** @var LogWriterService */
    private $logWriterService;

    /**
     * CrontabCommand constructor.
     * @param CrontabService $crontabService
     * @param LogWriterService $logWriterService
     * @param string|null $name
     */
    public function __construct
    (
        CrontabService $crontabService,
        LogWriterService $logWriterService,
        string $name = null
    ) {
        parent::__construct($name);
        $this->crontabService = $crontabService;
        $this->logWriterService = $logWriterService;
    }

    protected function configure()
    {
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws CrontabException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->crontabService->executeCron();
            $output->writeln(self::MESSAGE_SUCCESS_CRONTAB_COMMAND);

            return 0;
        } catch (CrontabException $e) {
            $this->logWriterService->logError($e->getMessage());
            throw $e;
        }
    }
}