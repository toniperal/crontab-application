<?php

namespace App\Infrastructure\Symfony\Command;

use App\Application\Service\Fixture\CrontabFixtureService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FixturesCommand
 * @package App\Infrastructure\Symfony\Command
 */
class FixturesCommand extends Command
{
    protected static $defaultName = 'fixtures:command';

    /** @var CrontabFixtureService */
    private $crontabFixtureService;

    /**
     * FixturesCommand constructor.
     * @param CrontabFixtureService $crontabFixtureService
     * @param string|null $name
     */
    public function __construct
    (
        CrontabFixtureService $crontabFixtureService,
        string $name = null
    ) {
        parent::__construct($name);
        $this->crontabFixtureService = $crontabFixtureService;
    }

    protected function configure()
    {
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Data loading begins...Please wait');
        $this->crontabFixtureService->chargeFixturesInBBDD();
        $output->writeln('Data upload has been completed successfully.');

        return 0;
    }
}