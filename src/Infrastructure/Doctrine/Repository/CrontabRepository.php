<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\Crontab;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class CrontabRepository
 * @package App\Repository
 */
class CrontabRepository extends ServiceEntityRepository
{
    /**
     * CrontabRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Crontab::class);
    }
}