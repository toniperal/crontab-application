<?php

namespace App\Application\Service\Fixture;

use App\Application\Manager\CrontabManager;
use App\Domain\Entity\Crontab;

/**
 * Class CrontabFixtureService
 * @package App\Application\Service\Fixture
 */
class CrontabFixtureService
{
    public const DATA_FIXTURES_CRONTAB = [
        ['timeCrontab' => '* * * * *', 'command' => 'php /var/www/html/bin/console log:writer:command'],
        ['timeCrontab' => '*/5 11 * * *', 'command' => 'php /var/www/html/bin/console log:writer:command'],
    ];

    /** @var CrontabManager */
    private $crontabManager;

    public function __construct(CrontabManager $crontabManager)
    {
        $this->crontabManager = $crontabManager;
    }

    public function chargeFixturesInBBDD(): void
    {
        $crontabFixturesCollection = [];

        foreach (self::DATA_FIXTURES_CRONTAB as $fixture) {
            $newCrontab = new Crontab();
            $newCrontab->setTimeCrontab($fixture['timeCrontab']);
            $newCrontab->setCommand($fixture['command']);
            $newCrontab->setStatusCrontab(Crontab::STATE_ACTIVATE_CRONTAB);
            $crontabFixturesCollection[] = $newCrontab;
        }

        $this->crontabManager->saveCollection($crontabFixturesCollection);
    }
}