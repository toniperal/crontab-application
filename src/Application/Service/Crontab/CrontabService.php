<?php

namespace App\Application\Service\Crontab;

use App\Application\Exception\CrontabException;
use App\Application\Manager\CrontabManager;
use App\Domain\Entity\Crontab;
use Jobby\Exception;
use Jobby\Jobby;

class CrontabService
{
    /** @var CrontabManager */
    private $crontabManager;

    public function __construct(CrontabManager $crontabManager)
    {
        $this->crontabManager = $crontabManager;
    }

    /**
     * @throws CrontabException
     */
    public function executeCron(): void
    {
        try {
            /** @var Crontab */
            $crontabCollection = $this->crontabManager->findBy(['statusCrontab' => Crontab::STATE_ACTIVATE_CRONTAB]);

            if ($crontabCollection) {
                $crontabExit = new Jobby();
                foreach ($crontabCollection as $crontab) {

                    $crontabExit->add(
                        $crontab->getId().'_'.$crontab->getCommand(),
                        [
                            'command' => $crontab->getCommand(),
                            'schedule' => $crontab->getTimeCrontab(),
                        ]
                    );
                }
                $crontabExit->run();
            }
        } catch (\Exception $e) {
            throw new CrontabException(CrontabException::CRONTAB_MESSAGE.$e->getMessage());
        }
    }
}