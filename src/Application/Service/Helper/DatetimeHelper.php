<?php

namespace App\Application\Service\Helper;

/**
 * Class DatetimeHelper
 * @package App\Application\Service\Helper
 */
class DatetimeHelper
{
    public const DEFAULT_TIMEZONE = 'Europe/Madrid';
    public const DATE_RIGHT_NOW = 'now';

    /**
     * @param string $timeZone
     * @return \DateTime
     * @throws \Exception
     */
    public function getCurrentDateTime(string $timeZone = self::DEFAULT_TIMEZONE): \DateTime
    {
        $dateTimeZone = new \DateTimeZone($timeZone);

        return new \DateTime(self::DATE_RIGHT_NOW, $dateTimeZone);
    }

    /**
     * @param string $format
     * @return string
     * @throws \Exception
     */
    public function getCurrentDateTimeAsString(string $format = 'Y-m-d H:i:s'): string
    {
        return $this->getCurrentDateTime()->format($format);
    }
}