<?php

namespace App\Application\Service\Helper;

use Psr\Log\LoggerInterface;

/**
 * Class LogWriterService
 * @package App\Application\Service\Helper
 */
class LogWriterService
{
    public const MESSAGE_LOG_WRITER_WITH_TIME_AND_DAY = 'written at the time and day ';

    /** @var LoggerInterface */
    private $crontabLogger;

    /**
     * LogWriterService constructor.
     * @param LoggerInterface $crontabLogger
     */
    public function __construct
    (
        LoggerInterface $crontabLogger
    ) {
        $this->crontabLogger = $crontabLogger;
    }

    /**
     * @param string $message
     */
    public function logCrontab(string $message): void
    {
        $this->crontabLogger->info($message);
    }

    /**
     * @param string $message
     */
    public function logError(string $message): void
    {
        $this->crontabLogger->error($message);
    }
}