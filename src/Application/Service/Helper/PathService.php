<?php

namespace App\Application\Service\Helper;

use Symfony\Component\HttpKernel\KernelInterface;

class PathService
{
    /** @var string */
    private $rootDirectoy;

    /**
     * PathService constructor.
     * @param KernelInterface $kernel
     */
    public function __construct
    (
        KernelInterface $kernel
    ) {
        $this->rootDirectoy = $kernel->getProjectDir();
    }

    /**
     * @return string
     */
    public function getBasePath(): string
    {
        return $this->rootDirectoy;
    }
}