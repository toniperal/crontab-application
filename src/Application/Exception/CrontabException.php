<?php

namespace App\Application\Exception;

/**
 * Class CrontabException
 * @package App\Application\Exception
 */
class CrontabException extends \Exception
{
    public const CRONTAB_MESSAGE = 'Crontab error: ';

    /**
     * CrontabException constructor.
     * @param null $field
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($field = null, $message = "", $code = 0, \Exception $previous = null)
    {
        if (empty($message)) {
            $message = self::CRONTAB_MESSAGE.$field;
        }
        parent::__construct($message, $code, $previous);
    }
}