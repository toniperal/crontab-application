<?php

namespace App\Application\Exception;

/**
 * Class LogWriterException
 * @package App\Application\Exception
 */
class LogWriterException extends \Exception
{
    public const LOG_WRITER_COMMAND_MESSAGE = 'LogWriterCommand error: ';

    /**
     * LogWriterException constructor.
     * @param null $field
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($field = null, $message = "", $code = 0, \Exception $previous = null)
    {
        if (empty($message)) {
            $message = self::LOG_WRITER_COMMAND_MESSAGE.$field;
        }
        parent::__construct($message, $code, $previous);
    }
}