<?php

namespace App\Application\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

/**
 * Class AbstractManager.
 */
abstract class AbstractManager implements ObjectManagerInterface
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * AbstractManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function clearEntityManager(): void
    {
        $this->entityManager->clear();
    }

    /**
     * @param string $class
     * @return ObjectRepository
     */
    protected function getRepository(string $class): ObjectRepository
    {
        return $this->getEntityManager()->getRepository($class);
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}
