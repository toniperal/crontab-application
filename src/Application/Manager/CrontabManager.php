<?php

namespace App\Application\Manager;

use App\Domain\Entity\Crontab;
use App\Infrastructure\Doctrine\Repository\CrontabRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CrontabManager
 * @package App\Application\Manager
 */
class CrontabManager extends AbstractManager
{
    /** @var CrontabRepository */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->repository = $this->getRepository(Crontab::class);
    }

    /**
     * Finds one Crontab by id
     * @param int $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return Crontab|object|null
     */
    public function findOneById(int $id, $lockMode = null, $lockVersion = null)
    {
        return $this->repository->find($id);
    }

    /**
     * Finds one Crontab by the given criteria
     * @param array $criteria
     * @param array|null $orderBy
     * @return Crontab|object|null
     */
    public function findOneBy(array $criteria, ?array $orderBy = null)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Finds Crontab by the given criteria
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array|Crontab[]
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * Creates an empty Crontab instance
     * @return Crontab
     */
    public function create(): Crontab
    {
        return new Crontab();
    }

    /**
     * Saves a Crontab collection
     * @param array|Crontab[] $crontabs
     * @return void
     */
    public function saveCollection(array $crontabs): void
    {
        $entityManager = $this->getEntityManager();
        foreach ($crontabs as $crontab) {
            $entityManager->persist($crontab);
        }
        $entityManager->flush();
    }

    /**
     * Removes a Crontab collection
     * @param array|Crontab[] $crontabs
     * @return void
     */
    public function removeCollection(array $crontabs): void
    {
        $entityManager = $this->getEntityManager();
        foreach ($crontabs as $crontab) {
            $entityManager->remove($crontab);
        }
        $entityManager->flush();
    }

    /**
     * Finds all Crontab
     * @return array|Crontab[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }


    /**
     * Saves a Crontab instance
     *
     * @param Crontab $crontab
     */
    public function save(Crontab $crontab): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($crontab);
        $entityManager->flush();
    }

    /**
     * Remove a Crontab instance
     *
     * @param Crontab $crontab
     */
    public function remove(Crontab $crontab): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($crontab);
        $entityManager->flush();
    }
}