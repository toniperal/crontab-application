<?php

namespace App\Application\Manager;

/**
 * Interface EntityManagerInterface
 * @package App\Application\Manager
 */
interface ObjectManagerInterface
{
    /**
     * Finds one entity by id
     * @param int $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return mixed
     */
    public function findOneById(int $id, $lockMode = null, $lockVersion = null);

    /**
     * Finds one entity by the given criteria
     * @param array $criteria
     * @param array|null $orderBy
     * @return mixed
     */
    public function findOneBy(array $criteria, ?array $orderBy = null);

    /**
     * Finds entities by the given criteria
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null): array;

    /**
     * Creates an empty entity instance
     * @return mixed
     */
    public function create();

    /**
     * Saves an entity collection
     * @param array $entities
     * @return void
     */
    public function saveCollection(array $entities): void;


    /**
     * Removes an entity collection
     * @param array $entities
     * @return void
     */
    public function removeCollection(array $entities): void;
}
