<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Doctrine\Repository\CrontabRepository")
 */
class Crontab
{
    public const STATE_DEACTIVATE_CRONTAB = 0;
    public const STATE_ACTIVATE_CRONTAB = 1;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="time_crontab", length=50)
     */
    private $timeCrontab;

    /**
     * @var int|null
     * @ORM\Column(type="integer", name="status_crontab", length=1)
     */
    private $statusCrontab;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $command;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Crontab
     */
    public function setId($id): Crontab
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeCrontab(): string
    {
        return $this->timeCrontab;
    }

    /**
     * @param string $timeCrontab
     * @return Crontab
     */
    public function setTimeCrontab(string $timeCrontab): Crontab
    {
        $this->timeCrontab = $timeCrontab;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatusCrontab(): ?int
    {
        return $this->statusCrontab;
    }

    /**
     * @param int|null $statusCrontab
     * @return Crontab
     */
    public function setStatusCrontab(?int $statusCrontab): Crontab
    {
        $this->statusCrontab = $statusCrontab;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     * @return Crontab
     */
    public function setCommand(string $command): Crontab
    {
        $this->command = $command;

        return $this;
    }
}