* [Beginning](../README.md)

### Important: after installation always access this way, otherwise it will not start the cron service.
```
docker-compose up -d
docker exec crontab_tp_apache /etc/init.d/cron start && docker exec -it crontab_tp_apache bash
```


#### If everything has gone well, we can view the web at the following address
```
http://localhost:8002/crontab/
```

## Tests from the container
```
php bin/phpunit tests/Command/CrontabCommandTest.php
php bin/phpunit tests/Command/LogWriterCommandTest.php
```

## Start up

- Here you do not have to do anything, it is simply informative. In the container there is created a cron 
that calls the crontab: command every minute, this executes the crontab defined in the application and if 
they are in execution time they will be executed.
```
* * * * * root php /var/www/html/bin/console crontab:command >> /var/log/cron.log 2>&1 
```

- Execution commands are defined in the app with time and command
```
http://localhost:8002/crontab/
```
- You can verify that the application is working by allowing 1 minute to pass 
after entering the container and see that the log file is located within the 
following path, if you view it every minute you should write a new line with the 
date and time. 
```
/var/www/html/var/log/crontab-2022-xx-xx.log
```