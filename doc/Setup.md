* [Beginning](../README.md)

## Installation
#### First we create and lift the docker container
```
docker-compose up -d --build
```
#### Second we go inside the container to install the dependencies
```
docker exec -it crontab_tp_apache bash
```
#### Third from the container we install the dependencies PHP
```
composer install
```
#### Fourth from the container we install the javascript dependencies
```
yarn && yarn build
```

#### Fifth from the container We prepare the DB with the following symfony command
```
php bin/console doctrine:schema:update --force
```

#### Sixth from the container we insert some data to the DB with the following symfony command
```
php bin/console fixtures:command
```
#### Seventh from the container we execute the following command to start the cron service
```
/etc/init.d/cron start
```

#### If everything has gone well, we can view the web at the following address
```
http://localhost:8002/crontab/
```